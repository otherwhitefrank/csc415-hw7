/*  Name: Frank Dye
	Date: 2/25/2014
	Description: A basic copy program using win32 system calls
*/

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 4096
#define COMMANDLINE_SIZE 2048
#define STRING_SIZE 512
#define MAX_ARGS 32

//Forward declarations
void printPrompt(HANDLE stdOutputHandle, const char* promptBuffer);

int main()
{
	//String constants
	const char szPrompt[] = "MyShell> ";
	const char szCmd[] = "cmd.exe /c ";

	int argc = 0;
	char* argv[MAX_ARGS];

	//szCommand is our formatted string 
	char szCommand[BUFFER_SIZE] = "";

	HANDLE stdOutHandle = NULL;
	HANDLE stdInputHandle = NULL;

	DWORD numBytesRead = 0;

	//Initialize STARTUPINFO and PROCESS
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	//Acquire stdout
	stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	if (stdOutHandle == INVALID_HANDLE_VALUE)
	{
		//Opening standard output failed.
		printf("Cannot acquire standard output!\n");
	}

	stdInputHandle = GetStdHandle(STD_INPUT_HANDLE);

	if (stdInputHandle == INVALID_HANDLE_VALUE)
	{
		//Opening standard input failed.
		printf("Cannot acquire standard input!\n");
	}

	do {
		//Buffers, szCommandBuffer == raw input from ReadFile
		char szCommandBuffer[BUFFER_SIZE];

		//Zero our temporary buffers
		ZeroMemory(szCommandBuffer, sizeof(szCommandBuffer));
		ZeroMemory(szCommand, sizeof(szCommand));

		//Append cmd.exe /c to szCommand
		strcat(szCommand, szCmd);

		//Display prompt
		printPrompt(stdOutHandle, szPrompt);

		//Read command from standard input
		ReadFile(stdInputHandle, &szCommandBuffer, BUFFER_SIZE, &numBytesRead, NULL);

		//Append a null character to make this a c-string
		szCommandBuffer[numBytesRead] = 0;

		char* token		= NULL;
		char* context	= NULL;
		char  delims[]	= " \t\n\r";

		// During the first read, establish the char string and get the first token.
		token = strtok_s(szCommandBuffer, delims, &context);
		argv[0] = token;

		// While there are any tokens left in szCommandBuffer
		argc = 0;
		while (token != NULL)
		{
			//Other times just concatenate
			strcat(szCommand,token);

			//Append a space
			strcat(szCommand, " ");

			// NOTE: NULL, function just re-uses the context after the first read.
			token = strtok_s(NULL, delims, &context); 
			argc++;
			argv[argc] = token;
		}

		//Create the process, taken from MSDN verbatim.
		if(!CreateProcess( NULL,   // No module name (use command line)
			szCommand,        // Command line
			NULL,           // Process handle not inheritable
			NULL,           // Thread handle not inheritable
			FALSE,          // Set handle inheritance to FALSE
			0,              // No creation flags
			NULL,           // Use parent's environment block
			NULL,           // Use parent's starting directory 
			&si,            // Pointer to STARTUPINFO structure
			&pi ))           // Pointer to PROCESS_INFORMATION struct
		{
			printf( "CreateProcess failed (%d).\n", GetLastError() );
		}

		// Wait until child process exits.
		WaitForSingleObject( pi.hProcess, INFINITE );

		// Close process and thread handles.
		CloseHandle( pi.hProcess );
		CloseHandle( pi.hThread );

	} while (!strcmp(szCommand,"cmd.exe /c exit ") == 0);

	CloseHandle(stdOutHandle);
	CloseHandle(stdInputHandle);

	return 0;
}


void printPrompt(HANDLE stdOutputHandle, const char* promptBuffer)
{
	DWORD numBytesWritten = 0;

	WriteFile(stdOutputHandle, promptBuffer, strlen(promptBuffer) , &numBytesWritten, NULL);

	if (numBytesWritten != strlen(promptBuffer))
	{
		printf("Error writing to standard out!");
		exit(1);
	}
}
