/*  Name: Frank Dye
Date: 2/22/2014
Description: A basic shell program using POSIX system calls
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>

#define BUFFER_SIZE 4096
#define STRING_SIZE 128

#define MAX_ARGS 32

int main(int argc, char** argv) {
    //Temporary buffers for text manipulation
    char szCmd[] = "MyShell> ";

    char szBuffer[BUFFER_SIZE];

    int bufferLength = 0;

    //Dynamic pointers for parsing commands
    char** myargv;
    int myargc = 0;

    //Declared outside of scope to allow loop invariant
    char* token = NULL;
    do {

        if (write(STDOUT_FILENO, szCmd, strlen(szCmd)) == -1) {
            //Error writing file
            printf("Error writing to standard output!");

            //Exit with error
            exit(1);
        }

        if (read(STDIN_FILENO, szBuffer, BUFFER_SIZE) == -1) {
            //Error reading from standard input
            printf("Error reading from standard output!");

            //Exit with error
            exit(1);
        }


        //Max number of parsed arguments is MAX_ARGS
        myargv = malloc(MAX_ARGS * sizeof (char*)); //Make an array of string pointers for our execvp

        /*
         GNU Standard Whitespace Characters
            ' '
             space
            '\f'
              formfeed
            '\n'
               newline
            '\r'
               carriage return
            '\t'
               horizontal tab
            '\v'
             vertical tab 
         */

        char* delim = " \f\n\r\t\v";

        myargc = 0;

        do {
            if (myargc == 0) {
                //First run of strtok takes full buffer
                token = strtok(szBuffer, delim);
            }
            else {
                //You have to call strtok with NULL to get subsequent tokens
                token = strtok(NULL, delim);
            }

            myargv[myargc] = token;
            myargc++;
        }
        while (token != NULL);

        int pid = fork(); //Fork process
        if (!pid) {
            //Child process
            if (execvp(myargv[0], myargv) == -1) {
                //Error in exec
                printf(strerror(errno));

            }
            _exit(0); // If exec fails then exit forked process.

        }
        else if (pid < 0) {
            //Failure to fork
            printf("%s : %s", "Failure to fork process", strerror(errno));
        }

        //Parent process, wait for child to finish
        waitpid(pid, NULL, 0);

        //store the first arg
        token = myargv[0];

        //Zero argument pointers
        int i = 0;
        for (i = 0; i < MAX_ARGS; i++)
            myargv[i] = 0;

    }
    while (strcmp(token, "exit") != 0);

	free(myargv);

    //exit program with error code 0
    return 0;
}

